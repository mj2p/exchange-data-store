#!/usr/bin/env python

import common
import json
from settings import settings
settings = settings()
from error import error
error = error()
import logging
import time
from datetime import datetime
from datetime import timedelta
import calendar
import hashlib
import hmac
import urllib
import sqlite3
from decimal import *

logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s - %(name) - %(levelname) - %(message)s',
                    datefmt='%m-%d %H:%M',
                    filename='exchange.log',
                    filemode='w')
#create the Logger
logger = logging.getLogger("bter")
logger.setLevel(logging.INFO)
# create console handler and set level to debug
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
# create formatter
formatter = logging.Formatter('%(levelname)s - %(name)s - %(message)s')
# add formatter to ch
ch.setFormatter(formatter)
# add ch to logger
logger.addHandler(ch)

class bter:

	#set the standard properties
	name = "bter"
	isActive = 1

	buy = {}
	buyAmount = {}
	sell = {}
	sellAmount = {}
	balance = {}
	ticker = {}
	depth = {}
	last = {}
	trades = {}

	def __init__(self):
		#load the settings from the database
		self.load()
		logger.debug("initialising")
		for cur in self.currencies:
			self.ticker[cur] = "http://data.bter.com/api/1/ticker/" + cur.lower() + "_btc"
			self.depth[cur] = "http://data.bter.com/api/1/depth/" + cur.lower() + "_btc"
		return

	def headers(self, params):
		if not self.key or not self.secret:
			self.load()
		return {"KEY": self.key, "SIGN": hmac.new(str(self.secret), str(urllib.urlencode(params)), digestmod=hashlib.sha512).hexdigest()}

	#load the database settings
	def load(self):
		#get the vicurex data from the database
		logger.debug("getting settings")
		#create sqlite connection object
		conn = sqlite3.connect("exchange.db")
		c = conn.cursor()
		#create the exchange_exchanges table if it doesn't exist
		c.execute("CREATE TABLE IF NOT EXISTS exchange_exchanges (exchange text, name text, value text)")
		#get the values
		#key
		c.execute("SELECT value FROM exchange_exchanges WHERE exchange=? AND name=?", (self.name, "key",))
		row = c.fetchone()
		if row is None:
			self.key = raw_input(self.name + " - Enter Key:")
		else:
			self.key = row[0]
		#secret
		c.execute("SELECT value FROM exchange_exchanges WHERE exchange=? AND name=?", (self.name, "secret",))
		row = c.fetchone()
		if row is None:
			self.secret = raw_input(self.name + " - Enter Secret:")
		else:
			self.secret = row[0]
		#fee
		#c.execute("SELECT value FROM exchange_exchanges WHERE exchange=? AND name=?", (self.name, "fee",))
		#row = c.fetchone()
		#if row is None:
		#	self.fee = raw_input(self.name + " - Enter Fee:")
		#else:
		#	self.fee = row[0]
#
#		#Currencies
#		#list of active currencies for this exchange
#		c.execute("SELECT currency FROM exchange_currencies WHERE exchange=?", (self.name,))
#		data = c.fetchall()
#		if not data:
		self.currencies = []
#		else:
#			self.currencies = []
#			for cur in data:
#				self.currencies.append(str(cur[0]))

		conn.commit()
		conn.close()
		self.save()
		return

	#save the settings back to the database
	def save(self):
		logger.debug("saving settings")
		#create sqlite connection object
		conn = sqlite3.connect("exchange.db")
		c = conn.cursor()
		#create the exchange_exchanges table if it doesn't exist
		c.execute("CREATE TABLE IF NOT EXISTS exchange_exchanges (exchange text, name text, value text)")
		#save the values
		#key
		c.execute("UPDATE exchange_exchanges SET value=? WHERE exchange=? AND name=?", (self.key, self.name, "key"))
		if c.rowcount == 0:
			c.execute("INSERT INTO exchange_exchanges (exchange,name,value) VALUES (?,?,?)", (self.name, "key", self.key))
		#secret
		c.execute("UPDATE exchange_exchanges SET value=? WHERE exchange=? AND name=?", (self.secret, self.name, "secret"))
		if c.rowcount == 0:
			c.execute("INSERT INTO exchange_exchanges (exchange,name,value) VALUES (?,?,?)", (self.name, "secret", self.secret))
		#fee
		#c.execute("UPDATE exchange_exchanges SET value=? WHERE exchange=? AND name=?", (self.fee, self.name, "fee"))
		#if c.rowcount == 0:
		#	c.execute("INSERT INTO exchange_exchanges (exchange,name,value) VALUES (?,?,?)", (self.name, "fee", self.fee))
		conn.commit()
		conn.close()
		return

	#get trade history
	def getTrades(self, pair):
		logger.debug("getting trade history")
		pair = pair.lower()
		params = {"pair": pair}
		headers = self.headers(params)
		res = common.APICall("POST", "https://bter.com/api/1/private/mytrades", params, headers)
		try:
			trades = json.loads(res)
		except ValueError, e:
			logger.warn("no JSON returned")
			return False
		print(trades)
		check = error.checkResponse(self, trades)
		if check == 1:
			return False

		self.trades = trades['trades']
		return True


	#get the balances
	def getBalances(self):
		logger.debug("getting balances")
		params = {}
		headers = self.headers(params)
		res = common.APICall("POST", "https://bter.com/api/1/private/getfunds", params, headers)
		try:
			bal = json.loads(res)
		except ValueError, e:
			logger.warn("no JSON returned")
			return False
		check = error.checkResponse(self, bal)
		if check == 1:
			self.balance['BTC'] = 0
			for cur in self.currencies:
				self.balance[cur] = 0
			return False
		self.balance['BTC'] = bal['available_funds']['BTC'] if 'BTC' in res else 0
		for cur in self.currencies:
			self.balance[cur] = bal['available_funds'][cur] if cur in res else 0
		return True

	#get open orders
	def getOpenOrders(self):
		params = {}
		headers = self.headers(params)
		res = common.APICall("POST", "https://bter.com/api/1/private/orderlist", params, headers)
		try:
			orders = json.loads(res)
		except ValueError, e:
			logger.warn("no JSON returned")
			return False
		check = error.checkResponse(self, orders)
		if check == 1:
			self.orders = ""
			return False
		self.orders = orders['orders']
		return True

	#trade
	def trade(self, pair, type, rate, amount):
		logger.debug("trading")
		d = datetime.utcnow()
		self.startTime = calendar.timegm(d.utctimetuple())
		params = {"pair": pair, "type": str(type), "rate": str(round(Decimal(rate), 8)), "amount": str(round(Decimal(amount),8))}
		logger.debug(params)
		headers = self.headers(params)
		try:
			trade = json.loads(common.APICall("POST", "https://bter.com/api/1/private/placeorder", params, headers))
		except ValueError, e:
			logger.warn("no JSON returned")
			return False
		check = error.checkResponse(self, trade)
		if check == 2:
			self.trade(pair, type, rate, amount)
			return
		if check == 1:
			return False
		return True

	def getCurrencyData(self, cur):
		try:
			val = json.loads(common.APICall("GET", self.depth[cur]))
		except ValueError, e:
			logger.warn("no JSON returned")
			return False
		check = error.checkResponse(self, val)
		if check == 2:
			self.getCurrencyData(cur)
			return
		if check == 1:
			self.buy[cur] = 0
			self.buyAmount[cur] = 0
			self.sell[cur] = 0
			self.sellAmount[cur] = 0
			return False
		self.buy[cur] = val['asks'][0][0] if 'asks' in val else 0
		self.buyAmount[cur] = val['asks'][0][1] if 'asks' in val else 0
		self.sell[cur] = val['bids'][0][0] if 'bids' in val else 0
		self.sellAmount[cur] = val['bids'][0][1] if 'bids' in val else 0
		return True

	#get the currency data with checks
	def getCurrency(self, cur):
		self.getCurrencyData(cur)
		logger.debug("getting currency data for " + cur)
		count = 0
		while (self.sell[cur] == 0 or self.buy[cur] == 0) and (count < 3):
			self.getCurrencyData(cur)
			count += 1
		return

	#the buy method
	def tradeBuy(self, cur, rate, amount=None):
		if self.trade(cur.lower() + "_btc", "BUY", rate, amount):
			logger.info("Buy Successful")
			return True
		else:
			logger.warn("Buy Failed")
			return False

	#the sell method
	def tradeSell(self, cur, rate, amount=None):
		if self.trade(cur.lower() + "_btc", "SELL", rate, amount):
			logger.info("Sell Successful")
			return True
		else:
			logger.warn("Sell Failed")
			return False
