#!/usr/bin/env python


from settings import settings
settings = settings()
import sqlite3
import sys
#from exchanges.btcE import btcE
from exchanges.bter import bter
#from exchanges.cryptsy import cryptsy
#from exchanges.cryptoTrade import cryptoTrade
#from exchanges.vircurex import vircurex
import logging
from decimal import *
import datetime
import time

#create the Logger
logger = logging.getLogger("exchange")
logger.setLevel(logging.DEBUG)
logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    datefmt='%m-%d %H:%M',
                    filename='exchange.log',
                    filemode='w')
# create console handler and set level to debug
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
# create formatter
formatter = logging.Formatter('%(levelname)s - %(message)s')
# add formatter to ch
ch.setFormatter(formatter)
# add ch to logger
logger.addHandler(ch)

outputfile = "trade_history.csv"
import os
if not os.path.isfile(outputfile):
	with open(outputfile, 'r+') as outfile:
		outfile.write('exchange,id,order_id,pair,type,rate,amount,time,time_unix\n')
	outfile.close()

exchanges = [bter()]

#create sqlite connection object
conn = sqlite3.connect("exchange.db")
c = conn.cursor()
#create the trades table if it doesn't exist
c.execute("CREATE TABLE IF NOT EXISTS trades (id INTEGER PRIMARY KEY, exchange TEXT, ex_id TEXT, order_id TEXT, pair TEXT, type TEXT, rate NUMERIC, amount NUMERIC, time TEXT, time_unix INTEGER)")
conn.commit()
conn.close()

#get the pair to search for
pair = raw_input("Enter the pair to get trades for (format XXX_YYY): ")

def saveTradeData(exchange, data):
	conn = sqlite3.connect('exchange.db')
	c = conn.cursor()

	#parse out the trade data
	for trade in data:
		c.execute('select id from trades where ex_id = ? and order_id = ?', (trade['id'], trade['orderid']))
		found = c.fetchone()
		#check if the trade already exists in the database
		if found is not None:
			continue
		#we didn't find a matching database entry so this is a new trade
		#pop it in the database
		logger.info('Saving order ' + trade['orderid'])
		c.execute('insert into trades (exchange, ex_id, order_id, pair, type, rate, amount, time, time_unix) values (?, ?, ?, ?, ?, ?, ?, ?, ?)',
		          (exchange, trade['id'], trade['orderid'], trade['pair'], trade['type'], trade['rate'], trade['amount'], trade['time'], trade['time_unix']))
		#save the same data to the output csv file
		with open(outputfile, 'a+') as outfile:
			outfile.write(exchange + ',' + trade['id'] + ',' + trade['orderid'] + ',' + trade['pair'] + ',' +
			              trade['type'] + ',' + trade['rate'] + ',' + trade['amount'] + ',' + trade['time'] + ',' + trade['time_unix'] + '\n')
		outfile.close()
	conn.commit()
	conn.close()
	return


try:
	while True:

		for ex in exchanges:

			logger.info("#################")
			logger.info("Exchange : " + ex.name)
			logger.info("#################")

			if ex.getTrades(pair) is True:
				saveTradeData(ex.name, ex.trades)

			time.sleep(300)


except KeyboardInterrupt:
	print "losing..."
	sys.exit()
