#!/usr/bin/env python

import sys
import logging

#create the Logger
logger = logging.getLogger("error")
logger.setLevel(logging.DEBUG)
logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    datefmt='%m-%d %H:%M',
                    filename='exchange.log',
                    filemode='w')
# create console handler and set level to debug
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
# create formatter
formatter = logging.Formatter('%(levelname)s - %(message)s')
# add formatter to ch
ch.setFormatter(formatter)
# add ch to logger
logger.addHandler(ch)

class error:

	#display a bad response message
	def statusError(self, res):
		logger.warn("Bad Response from server: " + str(res.status) + " " + res.reason)
		return

	#generic error output
	def error(self, ex, reason):
		sys.exit(ex.name + " - " + reason)
		return

	#check the response from each exchange for errors
	def checkResponse(self, ex, res):
		#generic bad response from APICall
		if 'badresponse' in res:
			logger.warn(ex.name + " - " + res['badresponse'])
			return 1

		if ex.name == "bter":
			if 'message' in res:
				logger.warn(ex.name + " - " + res['message'])
				return 1

		return 0
