#!/usr/bin/env python

import urllib2
import urllib
import sqlite3
from error import error
error = error()
import logging
from StringIO import StringIO
import gzip


#create the Logger
logger = logging.getLogger("common")
logger.setLevel(logging.INFO)
logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    datefmt='%m-%d %H:%M',
                    filename='exchange.log',
                    filemode='w')
# create console handler and set level to debug
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
# create formatter
formatter = logging.Formatter('%(levelname)s - %(message)s')
# add formatter to ch
ch.setFormatter(formatter)
# add ch to logger
logger.addHandler(ch)

def APICall(type, url, body=None, headers={}):
	headers['User-Agent'] = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36"
	headers['Connection'] = "keep-alive"
	headers['Accept'] = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8"
	headers['Accept-Encoding'] = "gzip,deflate,sdch"
	headers['Content-Type'] = "application/x-www-form-urlencoded"
	if type == "POST":
		logger.debug(url + "?" + urllib.urlencode(body))
		req = urllib2.Request(url, urllib.urlencode(body), headers)
	if type == "GET":
		if body:
			logger.debug(url + "?" + urllib.urlencode(body))
			req = urllib2.Request(url + "?" + urllib.urlencode(body), None, headers)
		else:
			logger.debug(url)
			req = urllib2.Request(url, None, headers)
	try:
		res = urllib2.urlopen(req)
		if res.getcode() != 200:
			error.statusError(res)
		if res.info().get('Content-Encoding') == 'gzip':
			data = gunzip(res)
		else:
			data = res.read()
		res.close()
		return data
	except urllib2.URLError, e:
		#logger.error("Bad response from server - " + str(e))
  		return '{"badresponse": "' + str(e) + '"}'


def getNonce(ex):
	#create sqlite connection object
	conn = sqlite3.connect("exchange.db")
	c = conn.cursor()
	#get the nonce value for the given exchange
	c.execute("SELECT value FROM exchange_nonces WHERE exchange=?", (ex.name,))
	row = c.fetchone()
	if row is None:
		nonce = 0
	else:
		nonce = row[0]
	#add one to the nonce value and write it back to the database
	nonce = int(nonce) + 1
	c.execute("UPDATE exchange_nonces SET value=? WHERE exchange=?", (nonce, ex.name))
	if c.rowcount == 0:
		c.execute("INSERT INTO exchange_nonces (exchange,value) VALUES(?,?)", (ex.name, nonce))
	conn.commit()
	conn.close()
	return nonce

def gunzip(res):
	buf = StringIO(res.read())
	f = gzip.GzipFile(fileobj=buf)
	data = f.read()
	return data
