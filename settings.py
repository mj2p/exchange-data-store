#!/usr/bin/env python

import sqlite3

class settings:

	def __init__(self):

		#create sqlite connection object
		conn = sqlite3.connect("exchange.db")
		c = conn.cursor()

		#create the exchange_settings table if it doesn't exist
		c.execute("CREATE TABLE IF NOT EXISTS exchange_settings (name text, value text)")

		#run
		#Boolean to indicate whether to run or not
		c.execute("SELECT value FROM exchange_settings WHERE name=?", ('run',))
		row = c.fetchone()
		if row is None:
			self.run = 1
		else:
			self.run = row[0]

		#tradeBuy
		#Boolean to indicate whether to actively trade or not
		c.execute("SELECT value FROM exchange_settings WHERE name=?", ('tradeBuy',))
		row = c.fetchone()
		if row is None:
			self.tradeBuy = 0
		else:
			self.tradeBuy = row[0]

		#tradeSell
		#Boolean to indicate whether to actively trade or not
		c.execute("SELECT value FROM exchange_settings WHERE name=?", ('tradeSell',))
		row = c.fetchone()
		if row is None:
			self.tradeSell = 0
		else:
			self.tradeSell = row[0]

		#deltaAmount
		#Value of the attempted purchase for a delta trade
		c.execute("SELECT value FROM exchange_settings WHERE name=?", ('deltaAmount',))
		row = c.fetchone()
		if row is None:
			self.deltaAmount = 1
		else:
			self.deltaAmount = row[0]

		#deltaBuyPercentage
		#Percentage of change at which delta trade are bought
		c.execute("SELECT value FROM exchange_settings WHERE name=?", ('deltaBuyPercentage',))
		row = c.fetchone()
		if row is None:
			self.deltaBuyPercentage = 5
		else:
			self.deltaBuyPercentage = row[0]

		#deltaSellPercentage
		#Percentage of change at which delta trade are sold
		c.execute("SELECT value FROM exchange_settings WHERE name=?", ('deltaSellPercentage',))
		row = c.fetchone()
		if row is None:
			self.deltaSellPercentage = 5
		else:
			self.deltaSellPercentage = row[0]

		#deltaMax
		#maximum number of delta trades to purchase on a falling price
		c.execute("SELECT value FROM exchange_settings WHERE name=?", ('deltaMax',))
		row = c.fetchone()
		if row is None:
			self.deltaMax = 5
		else:
			self.deltaMax = row[0]

		#startPrice
		#dont purchase deltas below this price
		c.execute("SELECT value FROM exchange_settings WHERE name=?", ('startPrice',))
		row = c.fetchone()
		if row is None:
			self.startPrice = 500
		else:
			self.startPrice = row[0]



		#small MA
		#value of small MA
		c.execute("SELECT value FROM exchange_settings WHERE name=?", ('smallMA',))
		row = c.fetchone()
		if row is None:
			self.smallMA = 15
		else:
			self.smallMA = row[0]

		#bigMA
		#value of bigMA
		c.execute("SELECT value FROM exchange_settings WHERE name=?", ('bigMA',))
		row = c.fetchone()
		if row is None:
			self.bigMA = 60
		else:
			self.bigMA = row[0]

		#close the db connection
		conn.commit()
		conn.close()

		#save the exchange_settings
		self.save()

		return

	def save(self):
		#save the exchange_settings when the exchange_settings object is closed
		conn = sqlite3.connect("exchange.db")
		c = conn.cursor()

		#run
		c.execute("UPDATE exchange_settings SET value=? WHERE name=?", (self.run, "run"))
		if c.rowcount == 0:
			c.execute("INSERT INTO exchange_settings (name,value) VALUES (?,?)", ("run", self.run))

		#TradeBuy
		c.execute("UPDATE exchange_settings SET value=? WHERE name=?", (self.tradeBuy, "tradeBuy"))
		if c.rowcount == 0:
			c.execute("INSERT INTO exchange_settings (name,value) VALUES (?,?)", ("tradeBuy", self.tradeBuy))

		#TradeSell
		c.execute("UPDATE exchange_settings SET value=? WHERE name=?", (self.tradeSell, "tradeSell"))
		if c.rowcount == 0:
			c.execute("INSERT INTO exchange_settings (name,value) VALUES (?,?)", ("tradeSell", self.tradeSell))

		#deltaAmount
		c.execute("UPDATE exchange_settings SET value=? WHERE name=?", (self.deltaAmount, "deltaAmount"))
		if c.rowcount == 0:
			c.execute("INSERT INTO exchange_settings (name,value) VALUES (?,?)", ("deltaAmount", self.deltaAmount))

		#deltaBuyPercentage
		c.execute("UPDATE exchange_settings SET value=? WHERE name=?", (self.deltaBuyPercentage, "deltaBuyPercentage"))
		if c.rowcount == 0:
			c.execute("INSERT INTO exchange_settings (name,value) VALUES (?,?)", ("deltaBuyPercentage", self.deltaBuyPercentage))

		#deltaSellPercentage
		c.execute("UPDATE exchange_settings SET value=? WHERE name=?", (self.deltaSellPercentage, "deltaSellPercentage"))
		if c.rowcount == 0:
			c.execute("INSERT INTO exchange_settings (name,value) VALUES (?,?)", ("deltaSellPercentage", self.deltaSellPercentage))

		#deltaMax
		c.execute("UPDATE exchange_settings SET value=? WHERE name=?", (self.deltaMax, "deltaMax"))
		if c.rowcount == 0:
			c.execute("INSERT INTO exchange_settings (name,value) VALUES (?,?)", ("deltaMax", self.deltaMax))

		#startPrice
		c.execute("UPDATE exchange_settings SET value=? WHERE name=?", (self.startPrice, "startPrice"))
		if c.rowcount == 0:
			c.execute("INSERT INTO exchange_settings (name,value) VALUES (?,?)", ("startPrice", self.startPrice))


		#smallMA
		c.execute("UPDATE exchange_settings SET value=? WHERE name=?", (self.smallMA, "smallMA"))
		if c.rowcount == 0:
			c.execute("INSERT INTO exchange_settings (name,value) VALUES (?,?)", ("smallMA", self.smallMA))

		#bigMA
		c.execute("UPDATE exchange_settings SET value=? WHERE name=?", (self.bigMA, "bigMA"))
		if c.rowcount == 0:
			c.execute("INSERT INTO exchange_settings (name,value) VALUES (?,?)", ("bigMA", self.bigMA))

		#close the db connection
		conn.commit()
		conn.close()

		return
